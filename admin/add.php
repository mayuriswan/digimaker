<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Digimaker | Admin</title>

    <!--Bootstrap core CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>


</head>

<body>
    <?php

    include('C:\xampp\htdocs\digimaker\frontpage\includes\databaser.php');
    if (isset($_POST) & !empty($_POST)) {
        if (empty($_POST['title'])) {
            $errors[] = "Title Field is required";
        }
    }


    ?>
    <?php
    if (empty($errors)) {
        $sql = "INSERT INTO posts (title,content,slug)
        VALUES ($title, :content, :slug)";
        $result = $db->prepare($sql);
        $values = array(
            ':title' => $_POST['title'],
            ':content' => $_POST['content'],
            ':slug' => $_POST['slug']
        );
    }

    ?>
    <nav class="navbar navbar-expand-lg navbar-default ">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">Digimaker</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="postes.php">Postes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pages.php">Pages</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="users.php">Users</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Welcom</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/frontpage/signIn.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!--Bootstrap core Javascript-->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
    <script>
        $('#editor1').editor1()
    </script>

    <header class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10">
                    <h2> <i class="bi bi-speedometer2"></i> Edit<small> mange your site </small> </h2>
                </div>
                <div class="col-md-2">
                    <div class="dropdown create">
                        <button class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Create
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Add Pages</a></li>
                            <li><a class="dropdown-item" href="add.php">Add articel</a></li>
                            <li><a class="dropdown-item" href="#">Add User</a></li>
                        </ul>
                        </li>
                    </div>
                </div>
            </div>
    </header>

    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="active">Add Page</li>
            </ol>
        </div>
    </section>

    <section id="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="index.php" class="list-group-item active main-color-bg">
                            <i class="bi bi-speedometer2"></i>
                            Dashboard
                        </a>
                        <a href="pages.php" class="list-group-item">
                            <i class="bi bi-file-earmark-break"></i> Pages</a>
                        <a href="postes.php" class="list-group-item">
                            <i class="bi bi-mailbox"></i> Postes</a>
                        <a href="users.php" class="list-group-item">
                            <i class="bi bi-people"></i> Users</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <?php
                        if (!empty($messages)) {
                            echo "<div class='alert alert sucess'>";
                            foreach ($messages as $message) {
                                echo "" . $message . "<br>";
                            }
                            echo "</div>";
                        }

                        ?>
                        <?php
                        if (!empty($errors)) {
                            echo "<div class='alert alert sucess'>";
                            foreach ($errors as $error) {
                                echo "" . $error . "<br>";
                            }
                            echo "</div>";
                        }

                        ?>
                        <div class="panel-heading main-color-bg">
                            <h3 class="panel-tittle">Add Page</h3>
                        </div>
                    </div>
                    <form role="form" method="post">
                        <div class="form-group">
                            <label>Page Tittle</label>
                            <input type="text" class="form-control" name="title" placeholder="Page Title" value="<?php if (isset($_POST['title'])) {
                                                                                                                        $title = $_POST['title'];
                                                                                                                        echo $title;
                                                                                                                    } ?>">
                        </div>
                        <div class="form-group">
                            <textarea id="editor" class="form-control" name="content" placeholder="Page body"><?php if (isset($_POST['content'])) {
                                                                                                                    $content = $_POST['content'];
                                                                                                                    echo $content;
                                                                                                                } ?>"></textarea>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Published
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Tags</label>
                            <input type="text" class="form-control" name="slug" placeholder="Add tag" value="<?php if (isset($_POST['slug'])) {
                                                                                                                    $slug = $_POST['slug'];
                                                                                                                    echo $slug;
                                                                                                                } ?>">">
                        </div>
                        <div class="form-group">
                            <label>Decription</label>
                            <input type="text" class="form-control" name="decription" placeholder="Add decription">
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" value="Submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        </div>
    </section>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
</body>

</html>