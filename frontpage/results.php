<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- navbar -->
    <?php include 'includes/Navbars/navbar.php'; ?>
    <!-- navbar ends -->

    <!-- home search -->
   
    <?php include 'includes/databaser.php';
    $sql = "SELECT * FROM results";
    $result = $db->prepare($sql);
    $result->execute();
    $posts = $result->fetchAll(PDO::FETCH_ASSOC);
    $nRows = $db->query('select count(*) from users')->fetchColumn(); 
    
    ?>
    
    <div class="flex flex-row m-24 mt-24">
  <div class="w-1/2 p-10 overflow-x-auto sm:-mx-6 lg:-mx-8 ml-10 ">
    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
    <h1 class="px-6 py-3 text-left text-4xl font-bold text-black-500 uppercase tracking-wider ">Resultater</h1>
    
      <h1 class="px-6 py-3 text-left text-xl font-medium text-black-500 uppercase tracking-wider "><span class="text-3xl font-bold text-red-500">2022</span><span class="text-3xl font-bold text-white-500"></h1>


      <div class=" sm:rounded-lg">
        
        <table class="min-w-full divide-y divide-gray-200">
          <thead class="bg-blue-500 border border-black ">
            <tr>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-white  border border-black  uppercase tracking-wider">vnan</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-white  border border-black  uppercase tracking-wider">Dato</th>
              

            </tr>
          </thead>
           
          <tbody class="bg-blue border border-blue-500 divide-y divide-gray-200">
          <?php 
            foreach($posts as $user){
                $sid='s' . $user['id'];
                echo "<tr> <td class='px-6 py-4 whitespace-nowrap text-sm  border border-black  font-medium text-gray-900'><div id='$sid' style='display:inline text:bold' onclick=display_detail($user[id])>$user[Nvan]</div></td>";

             ?> 
            
            

              
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900  border border-black "><?php echo $user['date']?></td>
              

             
            </tr>
            <?php }?>
            <!-- More people... -->
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php 
            foreach($posts as $user){
                $sid='s' . $user['id'];

             ?> 
            
  
  <div id="<?php echo $user['id']?>" class="display:inline w-1/2 h-full flex flex-row info relative ">

  
  <div class="flex-auto  overflow-hidden bg-white shadow sm:rounded-lg  w-full	 absolute right-0 ">
  
  
  <div id="<?php echo $user['id'].'t'?>" class=" w-auto h-auto border border-gray-100 "  >
              <h1 class=" p-5 uppercase text-2xl text-blue-700 text-bold"><?php echo  $user['Nvan']?></h1>
              <p class=" p-5 pt-0 text-blue-300 text-sm">Pbn html</p>
              <h1 class=" p-5 pt-0 uppercase text-lg text-black text-bold">Dato : <?php echo  $user['date']?></h1>
              <h1 class=" p-5 pt-0 uppercase text-lg text-black text-bold">Arrangor : <?php echo  $user['arranger']?></h1>
              <h1 class=" p-5 pt-0 uppercase text-lg text-black text-bold">Spill : <?php echo  $user['spill']?> , runder  : <?php echo  $user['runder']?> , par : <?php echo  $user['par']?></h1>


  
        <table class="min-w-full p-5 border border-black overflow-hidden divide-y divide-gray-200">
          <thead class="bg-blue-500 text-white">
            <tr>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium w-1/7 uppercase tracking-wider">Plass</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium w-1/7 uppercase tracking-wider">Par</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium w-2/7 uppercase tracking-wider">Nvan</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium w-1/7 uppercase tracking-wider">klubb</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium w-1/7  uppercase tracking-wider">poeng</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium w-1/7  uppercase tracking-wider">%</th>
    
            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
            <tr class="border border-gray-500">
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm font-medium text-gray-900">1</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">5 </td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">Anne-Ma Saebo - Astrid Kvakaaag</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">BK Hjerter Ess</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">27.00</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-green-500">64.06</td>
              
            </tr>
            <tr class="border border-gray-500">
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm font-medium text-gray-900">2</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">8</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">Merete Hoff - Mette Eeg</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">BK Hjerter Ess</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">20.00</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-green-500">60.42</td>
              
            </tr>

            <!-- More people... -->
          </tbody>
        </table>
     

  </div>
  </div>
</div>

  

  <?php }?>
</div>




    <?php include 'includes/footer.php'; ?>


</body>

</html>
<style>
    .info {
  display: none ;
}
.han {
  display: none;
}

    
</style>
<script>
    function display_detail(id){
      
    var sid='s'+id;
    if(document.getElementById(id).style.display == "inline" ){
    document.getElementById(id).style.display = 'none'; // Hide the details div

    

    }else {
   
    document.getElementById(id).style.backgroundColor = '#ffff00'; // Add different color to background
    document.getElementById(id).style.display = 'inline';  // show the details


    } // end of if else
    }
    function display_table(id){
      
      var sid=id+'d';
      var tid = id+'t';
      if(document.getElementById(sid).style.display == "block" ){

      document.getElementById(sid).style.display = 'none'; // Hide the details div
      document.getElementById(tid).style.display = 'block'; // Hide the details div

  
      }else {
     
      document.getElementById(sid).style.display = 'block';  // show the details
      document.getElementById(tid).style.display = 'none';  // show the details

  
  
      } // end of if else
      }
   
</script>