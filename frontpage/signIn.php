<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<html class="h-full bg-gray-50">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body class="h-full">
    <div class="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div class="w-full max-w-md space-y-8">
            <div>
                <img class="mx-auto h-14 w-15" src="img/i285134164477270418._szw3000h2000_.png" alt="Your Company">
                <h1 class="mt-6 text-center text-3xl font-bold tracking-tight text-blue-900">Hjerteress</h1>

                <h2 class="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">Logg Inn</h2>

            </div>
            <form class="mt-8 space-y-6" action="welcome.php" method="POST">
                <input type="hidden" name="remember" value="true">
                <div class="-space-y-px rounded-md shadow-sm">
                    <div>
                        <label for="epost-address" class="sr-only">Epost Adresse</label>
                        <input id="epost-address" name="email" type="email" autocomplete="email" required class="relative block w-full appearance-none rounded-none rounded-t-md
          border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500
          focus:outline-none focus:ring-indigo-500 sm:text-sm" placeholder="Epost address">
                    </div>
                    <div>
                        <label for="passord" class="sr-only">Passord</label>
                        <input id="passord" name="password" type="password" autocomplete="current-password" required class="relative
          block w-full appearance-none rounded-none rounded-b-md border
          border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500
          focus:outline-none focus:ring-indigo-500 sm:text-sm" placeholder="Passord">
                    </div>
                </div>

                <div class="flex items-center justify-between">
                    <div class="flex items-center">
                        <input id="husk-meg" name="remember-me" type="checkbox" class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500">
                        <label for="husk-meg" class="ml-2 block text-sm text-gray-900">Husk Meg</label>
                    </div>

                    <div class="text-sm">
                        <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">Glemt Passord?</a>
                    </div>
                </div>

                <div>
                    <a href="/digimaker/admin/index.php"><button type="button" class="btn btn-primary">Loginn</button>
                        <!--
                    
                    <a href="/digimaker/admin/index.php"> <button type="submit" class="group relative flex w-full justify-center
        rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none
        focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                            <span class="absolute inset-y-0 left-0 flex items-center pl-3">
                                 Heroicon name: mini/lock-closed 
                                <svg class="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z" clip-rule="evenodd" />
                                </svg>
                            </span>
                            Logg inn
                        </button> </a>
                        -->
                </div>
            </form>
        </div>
    </div>
</body>

</html>