<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- navbar -->
    <?php include 'includes/Navbars/welcomeNavbar.php'; ?>
    <!-- navbar ends -->

    <!-- home search -->
    <main>
        <div>
            <!-- Hero card -->
            <div class=" flex w-full   fixed top-1 z-10 ">
                <h1 class="w-1/3"> </h1>
                <h1 class="w-1/3"></h1>
                <h1 class="w-1/3  bg-black opacity-75 text-center h-24 pt-8 rounded-lg border-0 text-white sticky object-left top-0 z-10">
                    <?php echo "Spill mandager kl. 18.30 
            og torsdager kl. 10.30 fra mars 2022 i Voksen kirke (nedre inngang)." ?>
                </h1>
            </div>

            <div class="relative">

                <div class="absolute bottom-0 h-48 bg-gray-100"></div>
                <div class="max-w-full mx-auto ">
                    <div class="relative shadow-xl sm:rounded-2xl sm:overflow-hidden">
                        <div class="absolute inset-0 ">
                            <img class="h-full w-full object-fill " src="img/Home123.jpg" alt="People working on laptops">
                            <div class="absolute inset-0 bg-indigo-900 mix-blend-multiply opacity-0"></div>
                        </div>

                        <div class="relative px-4 py-16 sm:px-6 sm:py-24 lg:py-40 lg:px-8">
                            <h1 class="text-center font-extrabold tracking-tight ">
                                <span class=" block text-4xl mt-24 text-blue-700">Velkommen til Hjerteress</span>
                                <span class="block text-4xl mt-20 text-indigo-700"></span>
                            </h1>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Logo cloud -->


            <!-- More main page content here... -->
    </main>
    <!-- home search ends -->

    

    <?php include 'includes/footer.php'; ?>


</body>

</html>