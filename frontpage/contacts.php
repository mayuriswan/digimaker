
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- navbar -->
    <?php include 'includes/Navbars/navbar.php'; ?>
    <!--
  This example requires some changes to your config:
  
  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ],
  }
  ```
-->


<div class="hidden sm:block" aria-hidden="true">
  <div class="py-5">
    <div class="border-t border-gray-200 mt-15"></div>
  </div>
</div>

<div class="mt-10 sm:mt-0 h-92">
  <div class="md:grid md:grid-cols-3 md:gap-6 m-48 mt-1 bg-white">
    <div class="md:col-span-1">
      <div class="px-4 sm:px-0 m-20">
        <h3 class="text-2xl mb-2 font-medium leading-6 text-gray-900 ">Kontakts osss</h3>
        <p class="mt-1 text-sm text-black-600 mb-5">Har du et sposmail eller en tips , du er belkommen til a konkate oss ved a sender en mail till :</p>
        <p class="mt-1 text-sm text-blue-600 mb-5"><a href="#">hjerteress@support.no</a></p>
        <p class="mt-1 text-lg text-black-600 mb-5">Eller ved a ringe oss pa:</p>
        <p class="mt-1 text-lg text-black-600 mb-5"><span class="text-blue-700 text-bold text-xl mr-5 ">0047 4747474747</span><span class="text-green-600 text-xl text-bold " >Man-Fre: 08:30-21:00</span></p>
        <p class="mt-1 text-lg text-black-600 mb-5"><span  class="text-blue-700 text-bold text-xl mr-5 ">0047 85574622</span><span class="text-green-600 text-xl text-bold ">Man-Fre: 08:30-16:30</span></p>





      </div>
    </div>
    <div class="mt-5 md:col-span-2 md:mt-0 bg-gray-200 ">
      <form action="#" method="POST">
        <div class="overflow-hidden shadow sm:rounded-md h-3xl">
          <div class="bg-gray-100 px-4 py-5 sm:p-6">
            <div class="grid grid-cols-6 gap-6">
              <div class="col-span-6 sm:col-span-4">
                <label for="first-name" class="block text-sm font-medium text-gray-700">Nvan*</label>
                <input type="text" name="first-name" id="first-name" autocomplete="given-name" class="mt-1 block w-full rounded-md border p-3 border-black-900 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
              </div>

              <div class="col-span-6 sm:col-span-4">
                <label for="last-name" class="block text-sm font-medium text-gray-700">E-postadresse*</label>
                <input type="text" name="last-name" id="last-name" autocomplete="family-name" class="mt-1 block w-full rounded-md border p-3 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
              </div>
              <div class="col-span-6 sm:col-span-4">
                <label for="last-name" class="block text-sm font-medium text-gray-700">Telefonummer</label>
                <input type="text" name="last-name" id="last-name" autocomplete="family-name" class="mt-1 block w-full rounded-md border p-3 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
              </div>

              <div class="col-span-6 sm:col-span-4">
                <label for="last-name" class="block text-sm font-medium text-gray-700">Emne*</label>
                <input type="text" name="last-name" id="last-name" autocomplete="family-name" class="mt-1 block w-full rounded-md border p-3 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
              </div>
            

              <div class="col-span-6 sm:col-span-4">
                <label for="last-name" class="block text-sm font-medium text-gray-700">Melding*</label>
                <input type="text" name="last-name" id="last-name" autocomplete="family-name" class="mt-1 block w-full rounded-md border p-10 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
              </div>
            

              

              
            </div>
          </div>
          <div class="bg-gray-50 px-4 py-3 text-right sm:px-6">
            <button type="submit" class="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Send</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="hidden sm:block" aria-hidden="true">
  <div class="py-5">
    <div class="border-t border-gray-200"></div>
  </div>
</div>



     <?php include 'includes/footer.php'; ?>


</body>

</html>