<header>
    <div class="relative bg-white">
        <div class="flex justify-between items-center max-w-7xl mx-auto px-4 py-6 sm:px-6 md:justify-start md:space-x-10 lg:px-8">
            <div class="flex justify-center items-center  lg:flex-1">
                <div class="mr-2"> <a href="#">

                        <img class="h-50% w-10 flex-row " src="img/i285134164477270418._szw3000h2000_.png" alt="">

                    </a></div>
                <div class="text-center text-3xl font-extrabold tracking-tight sm:text-3xl lg:text-3xl">Hjerteress</div>
            </div>
            <div class="-mr-2 -my-2 md:hidden">
                <button type="button" class="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-expanded="false">
                    <span class="sr-only">Open menu</span>
                    <!-- Heroicon name: outline/menu -->
                    <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                </button>
            </div>
            <nav class="hidden  md:flex space-x-10">

            <?php include 'includes/databaser.php';
                 $sql = "SELECT * FROM navbaritems";
                 $result = $mysqli -> query($sql);
                 $items = $result -> fetch_all(MYSQLI_ASSOC);
                


                

                foreach ($items as $item) { ?>
                   <div class="menu text-black hover:text-red-500"> <a href="<?php echo $item["link"] ?>" class=" font-medium "><?php echo $item["name"]; ?>


                    </a></div>
                <?php } ?>



                <!--
              'Solutions' flyout menu, show/hide based on flyout menu state.
              Entering: "transition ease-out duration-200"
                From: "opacity-0 translate-y-1"
                To: "opacity-100 translate-y-0"
              Leaving: "transition ease-in duration-150"
                From: "opacity-100 translate-y-0"
                To: "opacity-0 translate-y-1"
            -->




            </nav>
            <div class="hidden md:flex items-center justify-end md:flex-1 lg:w-0 mr-20">
                <a href="signIn.php" class="whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900"> Sign in </a>
                <a href="#" class="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700"> Sign up </a>
            </div>

        </div>

        <!--
        Mobile menu, show/hide based on mobile menu state.
        Entering: "duration-200 ease-out"
          From: "opacity-0 scale-95"
          To: "opacity-100 scale-100"
        Leaving: "duration-100 ease-in"
          From: "opacity-100 scale-100"
          To: "opacity-0 scale-95"
      -->

    </div>
    <div class="absolute z-30 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
        <div class="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
            <div class="pt-5 pb-6 px-5">
                <div class="flex items-center justify-between">
                    <div class="mr-2"> <a href="#" class="flex items-center justify-between">

                            <img class="h-50% w-10 flex-row " src="img/i285134164477270418._szw3000h2000_.png" alt="">Hjerteress


                        </a></div>
                    <div class="-mr-2">
                        <button type="button" class="bg-white mobile-menu-button rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                            <span class="sr-only">Close menu</span>
                            <!-- Heroicon name: outline/x -->
                            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="mt-6 mobile-menu">

                </div>
            </div>
            <div class="py-6 px-5 mobile-menu1">
                <div class="grid grid-cols-2 gap-4">
                    <?php
                    $items = array(array("name" => "Forside", "link" => "index.php"), array("name" => "Resultat", "link" => "#"), array("name" => "Nyheter", "link" => "nyheter.php"), array("name" => "Kontakt oss/Tips", "link" => "#"), array("name" => "NBF", "link" => "https://www.bridge.no/"));

                    foreach ($items as $key) {
                    ?>
                        <a href="<?php echo $key["link"] ?>" class=" font-medium  hover:text-gray-900"><?php echo $key["name"]; ?>


                        </a>
                    <?php } ?>

                </div>
                <div class="mt-6">
                    <a href="#" class="w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700"> Sign up </a>
                    <p class="mt-6 text-center text-base font-medium text-gray-500">
                        Existing customer?
                        <a href="#" class="text-gray-900"> Sign in </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
</header>
<script>
    // Grab HTML Elements
    const btn = document.querySelector("button.mobile-menu-button");
    const menu = document.querySelector(".mobile-menu");
    const menu1 = document.querySelector(".mobile-menu1");


    // Add Event Listeners
    btn.addEventListener("click", () => {
        menu.classList.toggle("hidden");
        menu1.classList.toggle("hidden");
    });
    function navHighlight(elem, home, active) {
    var url = location.href.split('/'),
        loc = url[url.length -1],
        link = document.querySelectorAll(elem);
    for (var i = 0; i < link.length; i++) {
        var path = link[i].href.split('/'),
            page = path[path.length -1];
        if (page == loc || page == home && loc == '') {
            link[i].parentNode.className += ' ' + active;
            document.body.className += ' ' + page.substr(0, page.lastIndexOf('.'));
            }
        }
    }
navHighlight('.menu a', 'index.php', 'w-26 h-8  text-white flex items-center p-2 justify-center  border border-transparent rounded-md shadow-sm  font-medium text-white bg-indigo-600 hover:bg-indigo-700'); /* menu link selector, home page, highlight class */

</script>
