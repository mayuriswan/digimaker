<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- navbar -->
    <?php include 'includes/databaser.php';
    $sql = "SELECT * FROM posts";
    $result = $db->prepare($sql);
    $result = $mysqli -> query($sql);
    $posts = $result -> fetch_all(MYSQLI_ASSOC);
    ?>
    <?php include 'includes/Navbars/navbar.php'; ?>

    <!-- navbar ends -->

    <!-- home search -->

    <!-- home search ends -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 class="my-4">heading
                    <small>secondary text</small>
                </h1>
                <?php
                foreach ($posts as $post)
                ?>
                <div class="card mb-4">
                    <img class="card-img-top" src="" alt="Card Image cap">
                    <div class="card-body">
                        <h2 class="card-title"><?php echo $post['title']; ?></h2>
                        <p class="card-text"><?php echo $post['content']; ?></p>

                    </div>
                </div>
            </div>
        </div>
    </div>





</body>