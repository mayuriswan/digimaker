<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- navbar -->
    <?php include 'includes/Navbars/WelcomeNavbar.php'; ?>
    <!-- navbar ends -->

    <!-- home search -->
   
    <?php include 'includes/databaser.php';
    $sql = "SELECT * FROM users";
    $result = mysqli_query($conn, $sql);
    $posts = mysqli_fetch_assoc($result);
    
    ?>
    
    <div class="flex flex-row m-24 mt-24">
  <div class="w-1/2 p-10 my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 ml-10 ">
    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
    <h1 class="px-6 py-3 text-left text-4xl font-bold text-black-500 uppercase tracking-wider ">Medlemmer</h1>
    <div class=" mx-10 ">
        
        <table class="min-w-200  divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Antall</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Std</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">JR.</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">I.</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Lisens</th>
            </tr>
          </thead>
           
          <tbody class="bg-white divide-y divide-gray-200">
         
            <tr>
              <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900"><?php echo $nRows?></td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">2</td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">0</td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">57</td>
              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                0
              </td>
            </tr>
           
            <!-- More people... -->
          </tbody>
        </table>
      </div>
      <h1 class="px-6 py-3 text-left text-xl font-medium text-black-500 uppercase tracking-wider "><span class="text-3xl font-bold text-red-500">L</span>=<span class="text-3xl font-bold text-white-500">m/lisen</h1>
      <div class="relative mt-1 flex mb-6 ">
      <h1 class="px-6 py-3 flex-1 text-left text-xL ml-24 font-medium text-black-500  tracking-wider ">Sortter etter</h1>

    <button type="button" class="relative w-48  mr-56 cursor-default rounded-md border border-gray-300 bg-white py-2 pl-3 pr-10 text-left shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500 sm:text-sm" aria-haspopup="listbox" aria-expanded="true" aria-labelledby="listbox-label">
      <span class="flex items-center">
        <span class="ml-3 block truncate">Alfabetisk</span>
      </span>
      <span class="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
        <!-- Heroicon name: mini/chevron-up-down -->
        <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
          <path fill-rule="evenodd" d="M10 3a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02L10 4.852 7.3 7.76a.75.75 0 01-1.1-1.02l3.25-3.5A.75.75 0 0110 3zm-3.76 9.2a.75.75 0 011.06.04l2.7 2.908 2.7-2.908a.75.75 0 111.1 1.02l-3.25 3.5a.75.75 0 01-1.1 0l-3.25-3.5a.75.75 0 01.04-1.06z" clip-rule="evenodd" />
        </svg>
      </span>
    </button>
      </div>


      <div class=" sm:rounded-lg">
        
        <table class="min-w-full divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">ID</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">vnam</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Role</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Hcp</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Mp</th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-blue-500 uppercase tracking-wider">Email</th>

            </tr>
          </thead>
           
          <tbody class="bg-white divide-y divide-gray-200">
          <?php 
            foreach($posts as $user){
                $sid='s' . $user['id'];
                echo "<tr> <td class='px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900'><div id='$sid' style='display:inline text:bold' onclick=display_detail($user[id])>+ $user[id]</div></td>";

             ?> 
            
            

              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900"><?php echo $user['usernaem']?></td>
              
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900"><?php echo $user['role']?></td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">00</td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">00</td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900"><?php echo $user['email']?></td>

             
            </tr>
            <?php }?>
            <!-- More people... -->
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php 
            foreach($posts as $user){
                $sid='s' . $user['id'];

             ?> 
            
  
  <div id="<?php echo $user['id']?>" class="display:inline w-1/2 h-full flex flex-row info relative ">
<div class=" w-1/3 absolute left-0 top-0 bg-white shadow sm:rounded-lg  ">
<img class="bg-no-repeat bg-cover p-10 " src="img/pexels-photo-4380020.webp" alt="">
<p class="text-center text-xl font-bold">62.0 MP</p>
<p class="text-center">KLP =62 </p>


</div> 
  
  <div class="flex-auto  overflow-hidden bg-white shadow sm:rounded-lg  w-2/3	 absolute right-0 ">
  <div  id="<?php echo $user['id']?>" onclick=display_table(id) class=" flex flex-row border bg-gray-100 border-gray-500 display:inline ">
    <div class="w-1/2  shadow sm:rounded-lg px-4 py-5 sm:px-6 ">
    <h3 class="text-lg font-medium leading-6 text-gray-900">Informasjon</h3>
              
    </div class="flex bg-gray-100  flex-row border border-gray-500 ">
    <div class="w-1/2  shadow sm:rounded-lg px-4 py-5 sm:px-6">
    <h3 class="text-lg font-medium leading-6 text-gray-900  ">Handikap</h3>
              
    </div>
    
  </div>
  <div id="<?php echo $user['id'].'d'?>" class="  w-auto h-auto border border-gray-100 "  >
    <dl >
      <div class="bg-gray-50 px-4 border border-gray-200  py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">ID</dt>
        <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0"><?php echo  $user['id']?></dd>
      </div>
      <div class="bg-gray-50 px-4 border border-gray-200 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">fnavn</dt>
        <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0"><?php echo  $user['fnavn']?></dd>
      </div>
      <div class="bg-gray-50 px-4 border border-gray-200 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">enavn</dt>
        <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0"><?php echo  $user['enavn']?></dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid border-gray-200 sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">NAme</dt>
        <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0"><?php echo  $user['usernaem']?></dd>
      </div>
      <div class="bg-gray-50 px-4 border border-gray-200 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">E-POST</dt>
        <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0"><?php echo  $user['email']?></dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid border-gray-200 sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">Role</dt>
        <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0"><?php echo  $user['role']?></dd>
      </div>
     
    </dl>
  </div>
  <div id="<?php echo $user['id'].'t'?>" class="han  w-auto h-auto border border-gray-100 "  >
              <h1 class="text-center p-5 uppercase text-xl text-bold">Handikap</h1>
  
        <table class="max-w-full m-5 border border-black overflow-hidden divide-y divide-gray-200">
          <thead class="bg-blue-500 text-black">
            <tr>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium  uppercase tracking-wider">Dato</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium  uppercase tracking-wider">Turnening </th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium  uppercase tracking-wider">Plass</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium  uppercase tracking-wider">nytt</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium  uppercase tracking-wider">Gammed</th>
              <th scope="col" class="px-6 py-3 border border-gray-500 text-left text-xs font-medium  uppercase tracking-wider">Endring</th>
    
            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
            <tr class="border border-gray-500">
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm font-medium text-gray-900">2022-10-16</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">albir 200 ettern </td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">4</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">46.9</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">47.7</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-green-500">-0.84</td>
              
            </tr>
            <tr class="border border-gray-500">
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm font-medium text-gray-900">2022-10-14</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">albir 200 ettern </td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">28</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">47.9</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-gray-500">48.0</td>
              <td class="px-6 py-4 border border-gray-500 whitespace-nowrap text-sm text-green-500">-0.29</td>
              
            </tr>

            <!-- More people... -->
          </tbody>
        </table>
     

  </div>
  </div>
</div>

  

  <?php }?>
</div>




    <?php include 'includes/footer.php'; ?>


</body>

</html>
<style>
    .info {
  display: none ;
}
.han {
  display: none;
}

    
</style>
<script>
    function display_detail(id){
      
    var sid='s'+id;
    if(document.getElementById(id).style.display == "inline" ){
    document.getElementById(id).style.display = 'none'; // Hide the details div
    document.getElementById(sid).innerHTML = '+'+id;  // Change the symbol to + *

    

    }else {
   
    document.getElementById(id).style.backgroundColor = '#ffff00'; // Add different color to background
    document.getElementById(id).style.display = 'inline';  // show the details
    document.getElementById(sid).innerHTML = '-'+id; //Change the symbol to -


    } // end of if else
    }
    function display_table(id){
      
      var sid=id+'d';
      var tid = id+'t';
      if(document.getElementById(sid).style.display == "block" ){

      document.getElementById(sid).style.display = 'none'; // Hide the details div
      document.getElementById(tid).style.display = 'block'; // Hide the details div

  
      }else {
     
      document.getElementById(sid).style.display = 'block';  // show the details
      document.getElementById(tid).style.display = 'none';  // show the details

  
  
      } // end of if else
      }
   
</script>