<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hjerteress</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- navbar -->
    <?php include 'includes/Navbars/navbar.php'; ?>
    <!-- navbar ends -->

    <!-- home search -->
    <?php include 'includes/home_search.php'; ?>
    <!-- home search ends -->



    <?php include 'includes/footer.php'; ?>


</body>

</html>